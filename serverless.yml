service:
  name: 'jizo-accts'

frameworkVersion:  '>=1.72.0'

plugins:
  - serverless-webpack  
  - serverless-dynamodb-local
  - serverless-offline-sqs
  - serverless-offline  
  - serverless-localstack

custom:
  webpack:
    webpackConfig: './webpack.config.js'
    includeModules: true
  serverless-offline-sqs:
    autoCreate: false
    apiVersion: '2012-11-05'
    endpoint: http://0.0.0.0:4576
    region: us-east-1
    accessKeyId: root
    secretAccessKey: root
    skipCacheInvalidation: false    
  serverless-offline:
    stage:
      -local
    lambdaPort: 4574
  dynamodb:
    stages:
      - local      
    start:
      port: 4569
      inMemory: true
      heapInitial: 128m
      heapMax: 1g
      migrate: true
  localstack:
    docker:
      sudo: false
    debug: true
    endpoints:
      S3: http://localhost:4572
      DynamoDB: http://localhost:4569
      CloudFormation: http://localhost:4581
      Elasticsearch: http://localhost:4571
      ES: http://localhost:4578
      SNS: http://localhost:4575
      SQS: http://localhost:4576
      Lambda: http://localhost:4574
      Kinesis: http://localhost:4568
    host: "http://localhost"
    lambda:
      mountCode: false
    stages:
      - dev

provider:
  name: 'aws'
  apiGateway: 
    minimumCompressionSize: 1024
  environment:
    AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1'
    ML_EXT_LOOPBACK: 'NO'
    ML_EXT_DEBUG: 'YES'
    ML_EXT_SYSENV: ${opt:stage}
    ML_CLOUD_REGION: ${opt:region}       
  runtime: 'nodejs12.x'
  iamRoleStatements:
    - Effect: 'Allow'
      Action:
        - 'sqs:ReceiveMessage'
        - 'sqs:DeleteMessage'
      Resource:
        # TODO: add queues this service has to read from
        # - "arn:aws:sqs:::${self:service}-${opt:stage}-RescopeQueue"
        # - "arn:aws:sqs:::${self:service}-${opt:stage}-UncopeQueue"
    - Effect: 'Allow'
      Action:
        - 'sqs:SendMessage'
      Resource: '*'
    - Effect: 'Allow'
      Action:
        - 'dynamodb:DeleteItem'
        - 'dynamodb:PutItem'
        - 'dynamodb:Query'
        - 'dynamodb:Scan'
      Resource:
        # TODO: add tables this service has to read/write 
        # - "arn:aws:dynamodb:::jizo.${opt:stage}.accountsTable"        
    - Effect: 'Allow'
      Action: 
        - 's3:GetObject'
        - 's3:PutObject'
        - 's3:DeleteObject'
      Resource:
        # TOD: add the buckets this service has to read/write
        # - "arn:aws:s3:::${self:service}.${opt:stage}.identity"
    - Effect: 'Allow'
      Action:
        - 'sns:Publish'
      Resource:
        # TODO: add the topics this service as publish to
        # - "arn:aws:sns:::${self:service}-${opt:stage}-registerAccount"
        # - "arn:aws:sns:::${self:service}-${opt:stage}-unregisterAccount"
    - Effect: 'Allow'
      Action:
        - 'sns:Subscribe'
      Resource: '*'       
        
functions:
  sqsHandler:
    handler: src/sqs.handler
    description: "An example handler function"
    events:
      - sqs:
          arn: 
            Fn::GetAtt:
              - SqdExampleQueue
              - Arn            
  lambdaHandler:
    handler: src/lambda.handler    
    description: 'provide sync call to get user data'
  s3Handler: 
    handler: src/s3.handler
    description: "Trigger identity creation and validation when receiving files upload to bucket"
    events:
      - s3:
        bucket: "${self:service}.${opt:stage}.example"
        event: s3:ObjectCreate:*          
  router:
    handler: src/http.router
    description: 'primary REST related handlers for this service'
    events:
      - http: 'GET {proxy+}'
  
resources:
  Resources:
    # s3 configuration
    exampleBucket:
      Type: AWS::S3::Bucket
      Properties:
        BucketName: "${self:service}.${opt:stage}.example"

    # dynamodb configuration
    exampleTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: "jizo.${opt:stage}.exampleTable"
        AttributeDefinitions:
          - AttributeName: object_id
            AttributeType: S
        KeySchema:
          - AttributeName: object_id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1

    # incoming SQS configuration      
    exampleQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: "${self:service}-${opt:stage}-exampleQueue"
        VisibilityTimeout: 1080
        MessageRetentionPeriod: 2160
        RedrivePolicy:
          deadLetterTargetArn: 
            Fn::GetAtt:
              - exampleBackupQueue
              - Arn
          maxReceiveCount: 3
    exampleBackupQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: "${self:service}-${opt:stage}-exampleBackupQueue"

    # SNS Definitions
    exampleSnsTopic:
      Type: AWS::SNS::Topic
      Properties:
        TopicName: "${self:service}-${opt:stage}-exampleSnsTopic"