import { HttpErrorMiddlewareController } from "@leungas/lambda";
import { Request, Response } from 'lambda-api'

/**
 * @package @leungas/scaffold
 * @class ApplicationExceptionMiddlewareController
 * @description this is the extended handler for all exceptions that is specific for this package
 * @author Mark Leung <leungas@gmail.com>
 */
export class ApplicationExceptionMiddlewareController extends HttpErrorMiddlewareController {

    /**
     * a error code interpretor for handling
     * @param {Error} error     the error triggered
     * @return {number}
     */
    public static determine(error: Error) : number {
        let code: number = 500
        switch ((<any>error.constructor).name) {
            case "AccountAlreadyExistException": code = 423; break
            case "AccountTypeNotFoundException": code = 404; break
            case "IdentityTypeNotFoundException": code = 404; break
            case "UserAlreadyExistsException": code = 423; break
            case "UserFallbackException": code = 424; break            
            default: code = HttpErrorMiddlewareController.determine(error)
        }
        return code                   
    }

    /**
     * the main invoke function for the middleware
     * @param {Error} error     the error we received
     * @param {Request} req     the request that triggered 
     * @param {Response} res    the response coming out from the request
     * @param {Function} next   the next hop of our request
     */
    public static invoke(error: Error, req: Request, res: Response, next: () => void) {
        console.log("start error middleware")
        req = req
        let code: number = ApplicationExceptionMiddlewareController.determine(error)     
        console.error(error, error.stack)
        res.error(code, error.message, error.stack)
        next()
    }
}