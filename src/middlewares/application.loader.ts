import {Application} from '@leungas/aws-orm'
import {Request, Response} from 'lambda-api'

/**
 * @package @jizo/accounts
 * @class ApplicationLoaderMiddleware
 * @description trigger the loading of configuration into the middleware
 * @author Mark Leung <leungas@gmail.com>
 */
export class ApplicationLoaderMiddleware {

    /**
     * the main invoke function
     * @param {Request} req         the incoming request
     * @param {Response} res        the outgoing response
     * @param {Function} next       the next hop in middleware
     */
    public static invoke(req: Request, res: Response, next: () => void) {
        req = req; res = res
        const config = require('../configuration')
        Application.configure(config)
        next()
    }
}