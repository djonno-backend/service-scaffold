/**
 * this is the mount point for S3 related events.
 * (UNCOMMENT and edit handler class)
 */

import S3SampleEventController from "./controllers/s3/s3.sample";

// add your handlers here
exports.handler = S3SampleEventController.invoke