/**
 * this is the mount point for Lambda related events.
 * (UNCOMMENT and edit handler class)
 */

import { LambdaEventSampleController } from "./controllers";

// adding the event handlers here
exports.handler = LambdaEventSampleController.invoke
