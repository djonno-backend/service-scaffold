import {Context} from 'aws-lambda'
import {Application} from '@leungas/aws-orm'
import {ApplicationExceptionMiddlewareController} from '../..'

/**
 * @package @leungas/scaffold
 * @class LambdaEventSampleController
 * @description an example handler for lambda private functions (with no endpoint on API Gateway)
 * @author Mark Leung <leungas@gmail.com>
 */
export class LambdaEventSampleController {

    /**
     * our main invoke function to use
     * @param {any} event       the payload from the event
     * @param context 
     */
    public static async invoke(event: any, context: Context) {
        context = context
        Application.configure(require("../../configuration"))
        let response: object = {}
        try {
            let request: object = JSON.parse(event)
            //TODO: add your event handler tasks here
        } catch (ex) {
            console.log(ex, ex.stack)
            response = {
                status: "failed",
                data: {
                    code: ApplicationExceptionMiddlewareController.determine(ex),
                    message: ex
                }
            }
        } finally {
            return response
        }
    }
}