import {SQSEvent, Context, SQSRecord} from 'aws-lambda'
import {SqsEventController, SQSEventFailedException, RequestInvalidException} from '@leungas/lambda'
import {Application} from '@leungas/aws-orm'
import * as Requests from '@jizo/common/src/messages/requests/accounts'


/**
 * @package @leungas/scaffold
 * @class SqsSampleEventController
 * @description this is an example handler for taking SQS trigger events
 * @author Mark Leung <leungas@gmail.com>
 */
export class SqsSampleEventController {
    
    /**
     * the main action function on the record
     * @param {SQSRecord} record            the queue record we receiving
     * @return {any}
     */
    public static async process(record: SQSRecord) {
        if (record.body !== undefined) {
            // TODO: this is the main processing tasks.
        } else 
            throw new RequestInvalidException()
    }

    /**
     * this is our encapsulated method to trigger SQS event
     * @param {SQSEvent} event              the SQS records in event
     * @param {Context} context             the event trigger context
     * @return {any}
     */
    public static async invoke(event: SQSEvent, context: Context) {
        context = context
        Application.configure(require('../../configuration'))
        let failed: number = 0        
        await Promise.all(event.Records.map(async (record: SQSRecord) => {
            try {
                await SqsSampleEventController.process(record)
                await SqsEventController.clear(record)
            } catch (ex) {
                failed++
                console.error(ex, ex.stack)
            }
        }))
        if (failed > 0) 
            throw new SQSEventFailedException(failed)       
    }    
}