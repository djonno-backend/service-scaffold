import {HttpEventController} from "@leungas/lambda";
import {Request, Response} from 'lambda-api'
import {Token} from '@leungas/tokenizer'

/**
 * this is an example handler for HTTP REST request
 * @package @leungas/scaffold
 * @class HttpSampleEventController
 * @description this is an example of handling a HTTP REST event in lambda
 * @author Mark Leung <leungas@gmail.com>
 */
export class HttpSampleEventController {    

    /**
     * our controller invoke
     * @param {Request} req     the request received
     * @param {Response} res    the response to send back
     */
    public static async invoke(req: Request, res: Response) {        
        let token: Token = HttpEventController.getToken(req)            //  loading the session token        
        res.type("json")                                                //  determine the content type for response
        return {                                                        //  packaging response
            status: "success",                                          //      mark request as success
            data: {}                                                    //      our data to return on response
        }
    }
}