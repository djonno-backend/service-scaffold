import {S3Event, Context, S3EventRecord} from 'aws-lambda'
import {Application, Debugger} from '@leungas/aws-orm'
import {S3EventController} from '@leungas/lambda'

/**
 * @package @leungas/scaffold
 * @class S3SampleEventController
 * @description this is an example for handling S3 event triggers
 * @author Mark Leung <leungas@gmail.com>
 */
export default class S3SampleEventController {

    /**
     * this is our main invoke function to trigger
     * @param {S3Event} event                   our event fired
     * @param {Context} context                 the context we received the data
     * @return {Promise<any>}
     */
    public static async invoke(event: S3Event, context: Context) {
        Application.configure(require('../../configuration'))
        context = context
        event.Records.forEach(async (file: S3EventRecord) => {            
            let url: string = S3EventController.getSourcePath(file)
            Debugger.debug(`file stored in S3; ${url}`, event)
        })
    }
}