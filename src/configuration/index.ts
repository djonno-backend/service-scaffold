const dev = require("./dev")
const prod = require("./prod")
const local = require('./local')
export {dev, local, prod}