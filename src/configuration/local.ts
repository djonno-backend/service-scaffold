import {Environment} from '@leungas/aws-orm'

const application = "jizo"
const service = "accts"
const version = "1.0.0"
const localized = true
const env = Environment
const modules = { audit: false, notifications: require('./notiifications') }
const infrastructure = { 
    authentication: require('./infrastructure/authentication/dev.json'),
    loopback: {
        queues: require('./infrastructure/queues/dev.json')
    } 
}
const security = require('./security/dev.json')


export {application, service, version, localized, env, modules, infrastructure, security}