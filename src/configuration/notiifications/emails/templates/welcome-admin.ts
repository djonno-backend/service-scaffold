import { IMessaageTemplate } from "@leungas/emailer";

const en: IMessaageTemplate = {
    subject: "Salute! New account admin",
    body: "<p>Dear <%=data.name%>,</p>" +
          "<p>" +
            "Welcome to Jizo! As an administrator, you are given the access to maintain users, and settings of your account. But before you start " +
            "with these godly powers, there are a few things you'll need to do:" +
          "</p>" +
          "<li>Some things to do...</li>" +
          "<li>Some things to do...</li>" +
          "<li>Some things to do...</li>" +
          "<p>" +
            "Apart from that, visit our <a href=\"https://jizo.io/university\">university</a> to find out what amazing things you can do with Jizo " +
            "that makes your life much easier. And if you have any other topics you are not sure, we are also here to help." +
          "</p>" +
          "<p>Once again, it's great to have with you us and we look forward work together with you on your way to success!</p>" + 
          "<p><strong>Jizo Operations Team</strong></p>"
}

const ch: IMessaageTemplate = {
    subject: "對新的管理員，敬禮！",
    body: "<p><%=data.name%>，你好！</p>" +
          "<p>" + 
            "很感謝你選擇地藏App！作為一個戶口的管理員，你將擁有戶口的人員，設定及其他重要事項的最終決定的權限。但作出這些設定前，我們提議你先做以下幾件事：" +
          "</p>" +
          "<li>做事1</li>" + 
          "<li>做事1</li>" + 
          "<li>做事1</li>" + 
          "<p>" +
            "除此之外，你也可以到<a href=\"https://jizo.io/university\">地藏校園</a>查看更多地藏App能令你日常的更輕鬆的方法，把你的物業管理的工作可以更easy！" +
            "如果你在運作中遇到任可疑難，記住可以搵我地客戶服務幫手。" +
          "</p>" +
          "<p>我們再一次感謝你選擇地藏App，歡迎你加入我們的大家庭!" +
          "<p>祝你業務蒸蒸日上</p>" + 
          "<p><strong>《地藏》團隊</strong></p>"

}         

export {en, ch}