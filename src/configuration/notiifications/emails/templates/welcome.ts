import { IMessaageTemplate } from "@leungas/emailer"

const en: IMessaageTemplate = {
    subject: "Welcome to Jizo!",
    body:   "<p>Hi <%=data.name%>!</p>" + 
            "<p>" + 
                "Welcome to the Jizo community and we are having some really exciting features awaiting for you. But first, let's start " +
            "</p>"
}
const ch: IMessaageTemplate = {
    subject: "歡迎你使用地藏App",
    body:   "<p><%=data.name%>,你好！</p>" +
            "<p>" +
                "很感謝你選擇地藏App, 我們有一連串的工具正等待你！" +
            "</p>"
}

export { en, ch }