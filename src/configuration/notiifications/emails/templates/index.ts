import * as welcome from './welcome'
import * as welcomeAdmin from './welcome-admin'

export {welcome, welcomeAdmin}