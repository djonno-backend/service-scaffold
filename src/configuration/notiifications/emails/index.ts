import * as templates from './templates'
const sender: string = "leungas@gmail.com"

export {
    sender,
    templates
}