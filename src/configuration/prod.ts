import {Environment} from '@leungas/aws-orm'

const application = "jizo"
const service = "accts"
const version = "1.0.0"
const localized = false
const env = Environment
const modules = { audit: false, notifications: require('./notiifications') }
const infrastructure = { 
    authentication: require('./infrastructure/authentication/prod.json'),
    loopback: {
        queues: require('./infrastructure/queues/dev.json')
    } 
}
const security = require('./security/prod.json')

export {application, service, version, localized, env, modules, infrastructure, security}