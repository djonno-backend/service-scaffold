/**
 * this is the mount point for SQS related events.
 * (UNCOMMENT and edit handler class)
 */
import {SqsSampleEventController} from ".";

// adding the invokes for each of the events to happen
exports.handler = SqsSampleEventController.invoke