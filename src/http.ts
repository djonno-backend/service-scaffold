import createAPI from 'lambda-api'
import {APIGatewayEvent, Context} from 'aws-lambda'
import {HttpInternalUserMiddelware, SoftwareRecordGetController} from '@leungas/lambda'
import {ApplicationLoaderMiddleware, ApplicationExceptionMiddlewareController} from '.'

// loading lambda-api with version and logging
const api = createAPI({ version: "1.0", base: "v1", logger: true })         //  starting our api stack
api.use(ApplicationLoaderMiddleware.invoke)                                 //  loading our application configuration
api.use(ApplicationExceptionMiddlewareController.invoke)                    //  using the error 
    
api.get("about",                                                            //  GET: SWR retrieval  **
    HttpInternalUserMiddelware.invoke,                                      //      only for operator staff
    SoftwareRecordGetController.invoke);                                    //      pass to handler **


// this is the handler function for serverless
exports.router = async (event: APIGatewayEvent, context: Context) => {
    return await api.run(event, context)
}